require 'gem2deb/rake/spectask'

Gem2Deb::Rake::RSpecTask.new do |spec|
  spec.pattern = './spec/**/*_spec.rb'
  # disable tests that rely on a Forge testbed
  spec.exclude_pattern = [
    './spec/unit/forge/connection_spec.rb',
    './spec/integration/forge/v3/user_spec.rb',
    './spec/unit/forge/v3/user_spec.rb',
    './spec/integration/forge/v3/release_spec.rb',
    './spec/integration/forge/v3/module_spec.rb',
  ].join ','
end
